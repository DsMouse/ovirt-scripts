url_base="https://myserver.example.com"
url="${url_base}/ovirt-engine/api"
TAG=CTF2017
HREF="ovirt-engine/api"
user="admin@internal"
password="secretp@ss"


curl -o vms.xml \
         --insecure \
	-s -S\
         --header "Accept: application/xml" \
         --user "${user}:${password}" \
         "${url}/vms?search=tag%3D${TAG}"


for vm in $(xsltproc vms.xslt ./vms.xml | grep href | cut -f 2 -d '"' ) ; do 
  echo STARTING $vm
echo          "${url_base}/${vm}"
#curl -X POST -H "Accept: application/xml" -H "Content-Type: application/xml" -u [USER:PASS] --cacert [CERT] -d "<action><vm><os><boot dev='cdrom'/></os></vm></action>" https://[RHEVM Host]:8443/api/vms/6efc0cfa-8495-4a96-93e5-ee490328cf48/start

curl -o start.xml \
         --insecure \
	-s -S\
         --header "Accept: application/xml" \
         --user "${user}:${password}" \
         -X POST -H "Content-Type: application/xml" -d "<action><vm><os><boot/></os></vm></action>" \
         "${url_base}/${vm}"
rm -f start.xml
  done

echo Cleanign up 
#rm -vf tags.xml vms.xml start.xml 
