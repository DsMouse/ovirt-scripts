<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/vms">
    <xsl:for-each select="vm">
b_name="<xsl:apply-templates select="name"/>" 
    <xsl:for-each select="actions/link">
      <xsl:if test="@rel = 'stop'">
          b_href="<xsl:apply-templates select="@href"/>" 
      </xsl:if>
    </xsl:for-each>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>
